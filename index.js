require("dotenv").config();
const { PORT } = process.env;

const express = require('express');
const cors = require('cors');

const app = express();
const rootRouter = require('./routes');

app.use(cors());
app.use('/', rootRouter);

app.listen(
    PORT,
    () => console.info(`Back-end server listening at port ${PORT}.`)
);
