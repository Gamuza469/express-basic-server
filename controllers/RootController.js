const showGreeting = function (request, response) {
    response.json({
        message: 'Welcome!'
    });
};

module.exports = {
    showGreeting
};