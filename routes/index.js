const express = require('express');
const router = express.Router();

const RootController = require('../controllers/RootController');

router.get('/', RootController.showGreeting);

module.exports = router;
